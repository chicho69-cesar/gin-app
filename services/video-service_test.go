package services

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/chicho69-cesar/gin-app/entities"
)

const (
	TITLE = "Video Title"
	DESCRIPTION = "Video Description"
	URL = "https://www.youtube.com/watch?v=ILS4yYrxY6s&t=26s"
)

func getVideo() entities.Video {
	return entities.Video {
		Title: TITLE,
		Description: DESCRIPTION,
		URL: URL,
	}
}

func TestFindAll(t *testing.T) {
	service := New()
	service.Save(getVideo())

	videos := service.FindAll()
	firstVideo := videos[0]

	assert.NotNil(t, videos)
	assert.Equal(t, TITLE, firstVideo.Title)
	assert.Equal(t, DESCRIPTION, firstVideo.Description)
	assert.Equal(t, URL, firstVideo.URL)
}
