package main

import (
	"io"
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
	"gitlab.com/chicho69-cesar/gin-app/controllers"
	"gitlab.com/chicho69-cesar/gin-app/middlewares"
	"gitlab.com/chicho69-cesar/gin-app/services"

	// gindump "github.com/tpkeeper/gin-dump"
)

// inicializamos nuestros servicios y el controlador
var (
	videoService services.IVideoService = services.New()
	videoController controllers.IVideoController = controllers.New(videoService)
)

// establecemos una forma de llevar el control de los logs del server
func setLogOutput() {
	file, _ := os.Create("gin.log")
	gin.DefaultWriter = io.MultiWriter(file, os.Stdout)
}

func main() {
	setLogOutput() // arrancamos los logs

	// inicializamos un servidor
	server := gin.New()
	// esto es lo mismo que si crearamos la instancia del server con Default
	
	// cargarmos los archivos estaticos de la aplicacion
	server.Static("/css", "./templates/css")
	server.LoadHTMLGlob("templates/*.html")
	
	server.Use(gin.Recovery()) // usamos un middleware de recuperacion
	server.Use(middlewares.CORS()) // desahabilitamos los cors
	// server.Use(gin.Logger())
	server.Use(middlewares.Logger()) // middleware de loggeos
	// server.Use(middlewares.BasicAuth()) // middleware para authentication
	// server.Use(gindump.Dump()) // middleware para ver mas informacion en el debugging de la api, usa el package dump

	// creamos un endpoint el cual tiene acceso al contexto de Gin
	server.GET("/test", func(ctx *gin.Context) {
		// regresamos un objeto json con status 200
		ctx.JSON(200, gin.H{
			"message": "Ok!!!",
		})
	})

	apiRoutes := server.Group("/api") 
	{
		// endpoint get /videos
		apiRoutes.GET("/videos", func(ctx *gin.Context) {
			ctx.JSON(200, videoController.FindAll())
		})

		// endpoint post /videos
		apiRoutes.POST("/videos", func(ctx *gin.Context) {
			err := videoController.Save(ctx)
			if err != nil {
				ctx.JSON(http.StatusBadRequest, gin.H { "error": err.Error() })
			} else {
				ctx.JSON(http.StatusOK, gin.H { "message": "Video Input is valid" })
			}
		})
	}

	viewRoutes := server.Group("/view")
	{
		viewRoutes.GET("/videos", videoController.ShowAll)
	}

	// obtenemos el parametro PORT de la cli
	port := os.Getenv("PORT")
	if port == "" {
		port = "5000"
	} 

	// ejecutamos el servidor
	server.Run(":" + port)
}
