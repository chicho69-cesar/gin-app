package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/go-playground/validator/v10"
	"gitlab.com/chicho69-cesar/gin-app/entities"
	"gitlab.com/chicho69-cesar/gin-app/services"
	"gitlab.com/chicho69-cesar/gin-app/validators"
)

type IVideoController interface {
	FindAll() []entities.Video
	Save(ctx *gin.Context) error
	ShowAll(ctx *gin.Context)
}

type controller struct {
	service services.IVideoService
}

var validate *validator.Validate

func New(service services.IVideoService) IVideoController {
	validate = validator.New()
	validate.RegisterValidation("is-cool", validators.ValidateCoolTitle)
	
	return &controller {
		service: service,
	}
}

func (c *controller) FindAll() []entities.Video {
	return c.service.FindAll()
}

func (c *controller) Save(ctx *gin.Context) error {
	var video entities.Video
	
	err := ctx.ShouldBindJSON(&video) // es mas flexible que BindJSON
	if err != nil {
		return err
	}

	err = validate.Struct(video)
	if err != nil {
		return err
	}

	c.service.Save(video)
	
	return nil
}

func (c *controller) ShowAll(ctx *gin.Context) {
	videos := c.service.FindAll()
	data := gin.H {
		"title": "Video Page",
		"videos": videos,
	}

	ctx.HTML(http.StatusOK, "index.html", data)
}
